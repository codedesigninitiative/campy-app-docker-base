FROM lorisleiva/laravel-docker:latest
RUN apk add icu-dev icu-libs g++ && docker-php-ext-configure intl && docker-php-ext-install intl
